import React, { Component } from 'react'
import UserConsumer from "../contexts/QuestionContext";
import axios from "axios";



class UpdateUser extends Component {

  state = {
    question : "",
    optionA : "",
    optionB : "",
    optionC : "",
    optionD : "",
    trueOption : "",
    date : "",
    examType : ""
  } 
   
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  componentDidMount = async () => {
    const {id} = this.props.match.params;
    
    const response = await axios.get(`http://localhost:3004/questions/${id}`);

    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = response.data;

    this.setState({
      question : question,
      optionA : optionA,
      optionB : optionB,
      optionC : optionC,
      optionD : optionD,
      trueOption : trueOption,
      date : date,
      examType : examType
    });

  }
  validateForm = () => {
    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = this.state;
    if (question === "" || optionA === "" || optionB === "" || optionC === "" || optionD === "" || trueOption === "" || date === "" || examType === "") {
        return false;
    }
    return true;
    
}
  updateUser = async (dispatch,e) => {
      e.preventDefault();

      // Update User
      const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = this.state;
      const {id} = this.props.match.params;
      const updatedUser = {
        question,
        optionA,
        optionB,
        optionC,
        optionD,
        trueOption,
        date,
        examType
      };
      if (!this.validateForm()) {
        this.setState({
            error :true
        })
        return;
        }
      const response = await axios.put(`http://localhost:3004/Questions/${id}`,updatedUser);

      dispatch({type: "UPDATE_USER",payload : response.data});

      // Redirect
      this.props.history.push("/");
  } 
  render() {
    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType,error} = this.state;
    return <UserConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-8 mb-4">
              
                      
            
                      <div className="card">
                          <div className="card-header">
                          <h4>Update User Form</h4>
                          </div>
                          <div className="card-body">
                          {
                            error ? 
                            <div className = "alert alert-danger">
                               Lütfen bilgilerinizi kontrol edin.
                            </div>
                            :null
                         }
                              <form onSubmit = {this.updateUser.bind(this,dispatch)}>
                                  <div className="form-group">
                                      <label htmlFor="question">question</label>
                                      <input 
                                      type="text"
                                      name = "question"
                                      id = "id"
                                      placeholder = "Enter question"
                                      className ="form-control"
                                      value = {question}
                                      onChange = {this.changeInput}
              
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionA">optionA</label>
                                      <input 
                                      type="text"
                                      name = "optionA"
                                      id = "optionA"
                                      placeholder = "Enter optionA"
                                      className ="form-control"
                                      value = {optionA}
                                      onChange = {this.changeInput}
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionB">optionB</label>
                                      <input 
                                      type="text"
                                      name = "optionB"
                                      id = "optionB"
                                      placeholder = "Enter optionB"
                                      className ="form-control"
                                      value = {optionB}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionC">optionC</label>
                                      <input 
                                      type="text"
                                      name = "optionC"
                                      id = "optionC"
                                      placeholder = "Enter optionC"
                                      className ="form-control"
                                      value = {optionC}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionD">optionD</label>
                                      <input 
                                      type="text"
                                      name = "optionD"
                                      id = "optionD"
                                      placeholder = "Enter optionD"
                                      className ="form-control"
                                      value = {optionD}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="trueOption">trueOption</label>
                                      <input 
                                      type="text"
                                      name = "trueOption"
                                      id = "trueOption"
                                      placeholder = "Enter trueOption"
                                      className ="form-control"
                                      value = {trueOption}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="date">date</label>
                                      <input 
                                      type="text"
                                      name = "date"
                                      id = "date"
                                      placeholder = "Enter date"
                                      className ="form-control"
                                      value = {date}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="examType">examType</label>
                                      <input 
                                      type="text"
                                      name = "examType"
                                      id = "examType"
                                      placeholder = "Enter examType"
                                      className ="form-control"
                                      value = {examType}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <button className = "btn btn-danger btn-block" type = "submit">Update User</button>
                              
                              
                              </form>
                          </div>
                      
                      </div>
                      
                    </div>
                  )
            }
        }
    
    </UserConsumer>
    
    
    
    
    
  }
}
export default UpdateUser;
