import React, { Component } from 'react'
import ExamDatesConsumer from "../contexts/ExamDateContext";
import axios from "axios";



class UpdateExamDate extends Component {

  state = {
    examDate : ""
  } 
   
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  componentDidMount = async () => {
    const {id} = this.props.match.params;
    
    const response = await axios.get(`http://localhost:3004/examDates/${id}`);

    const {examDate} = response.data;

    this.setState({
        examDate : examDate
    });

  }
  validateForm = () => {
    const {examDate} = this.state;
    if (examDate === "") {
        return false;
    }
    return true;
    
}
updateExamDate = async (dispatch,e) => {
      e.preventDefault();

      // Update examDate
      const {examDate} = this.state;
      const {id} = this.props.match.params;
      const updatedeexamDate = {
        examDate
      };
      if (!this.validateForm()) {
        this.setState({
            error :true
        })
        return;
        }
      const response = await axios.put(`http://localhost:3004/examDates/${id}`,updatedeexamDate);

      dispatch({type: "UPDATE_EXAMDATE",payload : response.data});

      // Redirect
      this.props.history.push("/ExamDates");
  } 
  render() {
    const {examDate,error} = this.state;
    return <ExamDatesConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-8 mb-4">
                      <div className="card">
                          <div className="card-header">
                          <h4>Exam Tarihi Düzenle</h4>
                          </div>
                          <div className="card-body">
                          {
                            error ? 
                            <div className = "alert alert-danger">
                               Lütfen Tüm Alanları Doldurduğunuzdan Emin Olun.
                            </div>
                            :null
                         }
                              <form onSubmit = {this.updateExamDate.bind(this,dispatch)}>
                                  <div className="form-group">
                                      <label htmlFor="examDate">Değiştirmek istediğiniz sınav tarihi</label>
                                      <input 
                                      type="text"
                                      name = "examDate"
                                      id = "id"
                                      placeholder = "Enter examDate"
                                      className ="form-control"
                                      value = {examDate}
                                      onChange = {this.changeInput}
              
                                      />
                                  
                                  </div>
                                  <button className = "btn btn-warning btn-block" type = "submit">Kaydet</button>
                              
                              
                              </form>
                          </div>
                      
                      </div>
                      
                    </div>
                  )
            }
        }
    
    </ExamDatesConsumer>
  }
}
export default UpdateExamDate;
