import React, { Component } from 'react'
import QuestionConsumer from "../contexts/QuestionContext";
import axios from "axios";



class UpdateQuestion extends Component {

  state = {
    question : "",
    optionA : "",
    optionB : "",
    optionC : "",
    optionD : "",
    trueOption : "",
    date : "",
    examType : ""
  } 
   
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  componentDidMount = async () => {
    const {id} = this.props.match.params;
    
    const response = await axios.get(`http://localhost:3004/questions/${id}`);

    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = response.data;

    this.setState({
      question : question,
      optionA : optionA,
      optionB : optionB,
      optionC : optionC,
      optionD : optionD,
      trueOption : trueOption,
      date : date,
      examType : examType
    });

  }
  validateForm = () => {
    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = this.state;
    if (question === "" || optionA === "" || optionB === "" || optionC === "" || optionD === "" || trueOption === "" || date === "" || examType === "") {
        return false;
    }
    return true;
    
}
  updateQuestion = async (dispatch,e) => {
      e.preventDefault();

      // Update 
      const {question,optionA,optionB,optionC,optionD,trueOption,date,examType} = this.state;
      const {id} = this.props.match.params;
      const updateQuestion = {
        question,
        optionA,
        optionB,
        optionC,
        optionD,
        trueOption,
        date,
        examType
      };
      if (!this.validateForm()) {
        this.setState({
            error :true
        })
        return;
        }
      const response = await axios.put(`http://localhost:3004/Questions/${id}`,updateQuestion);

      dispatch({type: "UPDATE_QUESTİON",payload : response.data});

      // Redirect
      this.props.history.push("/");
  } 
  render() {
    const {question,optionA,optionB,optionC,optionD,trueOption,date,examType,error} = this.state;
    return <QuestionConsumer>
        {
            value => {
                const {dispatch} = value;
                const {examTypes} = value;
                const {examDates} = value;

                let examTypesList = examTypes.map(examType => {
                  return (
                      <option key={examType.id} value={examType.examName}>{examType.examName}</option>
                  )
              });
              let examDatesList = examDates.map(examdate => {
                return (
                    <option key={examdate.id} value={examdate.examDate}>{examdate.examDate}</option>
                )
            });
                return (
     
                    <div className = "col-md-8 mb-4">
              
                      
            
                      <div className="card">
                          <div className="card-header">
                          <h4>Soruyu Düzenle</h4>
                          </div>
                          <div className="card-body">
                          {
                            error ? 
                            <div className = "alert alert-danger">
                               Lütfen Tüm Alanları Doldurduğunuzdan Emin Olun.
                            </div>
                            :null
                         }
                              <form onSubmit = {this.updateQuestion.bind(this,dispatch)}>
                                  <div className="form-group">
                                      <label htmlFor="question">Soru:</label>
                                      <input 
                                      type="text"
                                      name = "question"
                                      id = "id"
                                      placeholder = "Enter question"
                                      className ="form-control"
                                      value = {question}
                                      onChange = {this.changeInput}
              
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionA">A:</label>
                                      <input 
                                      type="text"
                                      name = "optionA"
                                      id = "optionA"
                                      placeholder = "Enter optionA"
                                      className ="form-control"
                                      value = {optionA}
                                      onChange = {this.changeInput}
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionB">B:</label>
                                      <input 
                                      type="text"
                                      name = "optionB"
                                      id = "optionB"
                                      placeholder = "Enter optionB"
                                      className ="form-control"
                                      value = {optionB}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionC">C:</label>
                                      <input 
                                      type="text"
                                      name = "optionC"
                                      id = "optionC"
                                      placeholder = "Enter optionC"
                                      className ="form-control"
                                      value = {optionC}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="optionD">D:</label>
                                      <input 
                                      type="text"
                                      name = "optionD"
                                      id = "optionD"
                                      placeholder = "Enter optionD"
                                      className ="form-control"
                                      value = {optionD}
                                      onChange = {this.changeInput}
                                      />
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="trueOption">Doğru Cevap:</label>
                                      <select
                                                name="trueOption"
                                                id="trueOption"
                                                onChange={this.changeInput}
                                                value={trueOption}
                                                className="form-control"
                                            >
                                                <option>A</option>
                                                <option>B</option>
                                                <option>C</option>
                                                <option>D</option>
                                            </select>
                                      {/* <input 
                                      type="text"
                                      name = "trueOption"
                                      id = "trueOption"
                                      placeholder = "Enter trueOption"
                                      className ="form-control"
                                      value = {trueOption}
                                      onChange = {this.changeInput}
                                      /> */}
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="date">Tarih:</label>
                                      <select
                                                name="date"
                                                id="date"
                                                onChange={this.changeInput}
                                                value={date}
                                                className="form-control"
                                            >
                                                {examDatesList}
                                            </select>
                                      {/* <input 
                                      type="text"
                                      name = "date"
                                      id = "date"
                                      placeholder = "Enter date"
                                      className ="form-control"
                                      value = {date}
                                      onChange = {this.changeInput}
                                      /> */}
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="examType">Sınav Türü:</label>
                                      <select
                                                name="examType"
                                                id="examType"
                                                onChange={this.changeInput}
                                                value={examType}
                                                className="form-control"
                                            >
                                                {examTypesList}
                                            </select>

                                      {/* <input 
                                      type="text"
                                      name = "examType"
                                      id = "examType"
                                      placeholder = "Enter examType"
                                      className ="form-control"
                                      value = {examType}
                                      onChange = {this.changeInput}
                                      /> */}
                                  </div>
                                  <button className = "btn btn-warning btn-block" type = "submit">Kaydet</button>
                              
                              
                              </form>
                          </div>
                      
                      </div>
                      
                    </div>
                  )
            }
        }
    
    </QuestionConsumer>
    
    
    
    
    
  }
}
export default UpdateQuestion;
