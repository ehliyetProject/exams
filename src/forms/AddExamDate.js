import React, { Component } from 'react';
import ExamDatesConsumer from "../contexts/ExamDateContext";
import axios from "axios";

class AddExamDate extends Component {

  state = {
    examDate: "",
    error : false
  }
  validateForm = () => {
    const {examDate} = this.state;
    if (examDate === "") {
          return false;
      }
      return true;
      
  }
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  
  addExamType = async (dispatch,e) => {
      e.preventDefault();
      
      const {examDate } = this.state;

      const newExamDate = {
        examDate : examDate
      }
      
      if (!this.validateForm()) {
          this.setState({
              error :true
          })
          return;
      }
      
      
      const response = await axios.post("http://localhost:3004/examDates",newExamDate);


      dispatch({type : "ADD_EXAMDATE",payload:response.data});

      // Redirect
      this.props.history.push("/ExamDates");
      
  } 
  render() {
    const {examDate,error} = this.state;
    return <ExamDatesConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-12 mb-4">
                      <div className="card">
                          <div className="card-header">
                          <h4>Sınav Tarihi Ekle</h4>
                          </div>
                          
                          <div className="card-body">
                             {
                                 error ? 
                                 <div className = "alert alert-danger">
                                    Lütfen Sınav Tarihi Giriniz.
                                 </div>
                                 :null
                             }

                              <form onSubmit = {this.addExamType.bind(this,dispatch)}>

                              <div className="form-group">
                                      <label htmlFor="examType">Sınav Tarihi</label>
                                      <input 
                                      type="text"
                                      name = "examDate"
                                      id = "id"
                                      placeholder = "Lütfen Sınav Tarihi Giriniz..."
                                      className ="form-control"
                                      value = {examDate}
                                      onChange = {this.changeInput}
              
                                      />
                                  </div>
                                  <button className = "btn btn-success btn-block" type = "submit">Sınav Tarihi Ekle</button>
                              </form>
                          </div>
                      
                      </div>
                    </div>
                  )
            }
        }
    
    </ExamDatesConsumer>
    
    
    
    
    
  }
}
export default AddExamDate;