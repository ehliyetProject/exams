import React, { Component } from 'react'
import ExamTypesConsumer from "../contexts/ExamTypesContext";
import axios from "axios";



class UpdateExamTypes extends Component {

  state = {
    examName : ""
  } 
   
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  componentDidMount = async () => {
    const {id} = this.props.match.params;
    
    const response = await axios.get(`http://localhost:3004/examTypes/${id}`);

    const {examName} = response.data;

    this.setState({
    examName : examName
    });

  }
  validateForm = () => {
    const {examName} = this.state;
    if (examName === "") {
        return false;
    }
    return true;
    
}
  updateExamType = async (dispatch,e) => {
      e.preventDefault();

      // Update examName
      const {examName} = this.state;
      const {id} = this.props.match.params;
      const updatedexamName = {
        examName
      };
      if (!this.validateForm()) {
        this.setState({
            error :true
        })
        return;
        }
      const response = await axios.put(`http://localhost:3004/examTypes/${id}`,updatedexamName);

      dispatch({type: "UPDATE_EXAMTYPES",payload : response.data});

      // Redirect
      this.props.history.push("/examTypes");
  } 
  render() {
    const {examName,error} = this.state;
    return <ExamTypesConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-8 mb-4">
                      <div className="card">
                          <div className="card-header">
                          <h4>Exam Türü Değiştir</h4>
                          </div>
                          <div className="card-body">
                          {
                            error ? 
                            <div className = "alert alert-danger">
                               Lütfen Tüm Alanları Doldurduğunuzdan Emin Olun.
                            </div>
                            :null
                         }
                              <form onSubmit = {this.updateExamType.bind(this,dispatch)}>
                                  <div className="form-group">
                                      <label htmlFor="examName">Değiştirmek istediğiniz sınav türü</label>
                                      <input 
                                      type="text"
                                      name = "examName"
                                      id = "id"
                                      placeholder = "Enter examName"
                                      className ="form-control"
                                      value = {examName}
                                      onChange = {this.changeInput}
              
                                      />
                                  
                                  </div>
                                  <button className = "btn btn-warning btn-block" type = "submit">Kaydet</button>
                              
                              
                              </form>
                          </div>
                      
                      </div>
                      
                    </div>
                  )
            }
        }
    
    </ExamTypesConsumer>
  }
}
export default UpdateExamTypes;
