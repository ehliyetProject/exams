import React, { Component } from 'react';
import ExamTypesConsumer from "../contexts/ExamTypesContext";
import axios from "axios";

class AddExamType extends Component {

  state = {
    examName : "",
    error : false
  }
  validateForm = () => {
    const {examName} = this.state;
    if (examName === "") {
          return false;
      }
      return true;
      
  }
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  
  addExamType = async (dispatch,e) => {
      e.preventDefault();
      
      const {examName } = this.state;

      const newExamType = {
        examName : examName
      }
      
      if (!this.validateForm()) {
          this.setState({
              error :true
          })
          return;
      }
      
      
      const response = await axios.post("http://localhost:3004/examTypes",newExamType);


      dispatch({type : "ADD_EXAMTYPES",payload:response.data});

      // Redirect
      this.props.history.push("/examTypes");
      
  } 
  render() {
    const {examName,error} = this.state;
    return <ExamTypesConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-12 mb-4">
                      <div className="card">
                          <div className="card-header">
                          <h4>Sınav Türü Ekle</h4>
                          </div>
                          
                          <div className="card-body">
                             {
                                 error ? 
                                 <div className = "alert alert-danger">
                                    Lütfen Sınav Adını Giriniz.
                                 </div>
                                 :null
                             }

                              <form onSubmit = {this.addExamType.bind(this,dispatch)}>

                              <div className="form-group">
                                      <label htmlFor="examType">Sınav Türü</label>
                                      <input 
                                      type="text"
                                      name = "examName"
                                      id = "id"
                                      placeholder = "Lütfen Sınav Adını Giriniz..."
                                      className ="form-control"
                                      value = {examName}
                                      onChange = {this.changeInput}
              
                                      />
                                  </div>
                                  <button className = "btn btn-success btn-block" type = "submit">Sınav Türünü Ekle</button>
                              </form>
                          </div>
                      
                      </div>
                    </div>
                  )
            }
        }
    
    </ExamTypesConsumer>
    
    
    
    
    
  }
}
export default AddExamType;