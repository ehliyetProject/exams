import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {QuestionProvider} from "./contexts/QuestionContext";
import {ExamTypesProvider} from "./contexts/ExamTypesContext";
import {ExamDatesProvider} from "./contexts/ExamDateContext";
import Login from './components/Login';

ReactDOM.render(
    <Login>
    <ExamDatesProvider>
    <ExamTypesProvider>
    <QuestionProvider>
    <App />
    </QuestionProvider>
    </ExamTypesProvider>
    </ExamDatesProvider>
    </Login>
    , 
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
