import React from 'react'
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import logo from './menu-logo.png';

function Navbar({ title }) {

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="/">
        <div className="logo">
          <img src={logo} alt="jsx-a11y/img-redundant-alt" width="50" height="50" />
        </div>
      </a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">

          <li className="nav-item dropdown">
            <div className="dropdown">
              <button type="button" className="btn btn-light dropdown-toggle" data-toggle="dropdown">
                Sorular
    </button>
              <div className="dropdown-menu">
                <Link to="/" className="dropdown-item">Sorular</Link>

                <div className="dropdown-divider"></div>
                <Link to="/addQuestion" className="dropdown-item">Soru Ekle</Link>
              </div>
            </div>
          </li>

          <li className="nav-item dropdown">
            <div className="dropdown">
              <button type="button" className="btn btn-light dropdown-toggle" data-toggle="dropdown">
                Sınav Tarihleri
    </button>
              <div className="dropdown-menu">
                <Link to="/ExamDates" className="dropdown-item">Sınav Tarihleri</Link>
                <div className="dropdown-divider"></div>
                <Link to="/AddExamDates" className="dropdown-item">Sınav Tarihi ekle</Link>
              </div>
            </div>
          </li>

          <li className="nav-item dropdown">
            <div className="dropdown">
              <button type="button" className="btn btn-light dropdown-toggle" data-toggle="dropdown">
                Sınav Türleri
    </button>
              <div className="dropdown-menu">
                <Link to="/examTypes" className="dropdown-item">Sınav Türleri</Link>
                <div className="dropdown-divider"></div>
                <Link to="/addExamType" className="dropdown-item">Sınav Türü Ekle</Link>
              </div>
            </div>
          </li>
        </ul>
        <span className="navbar-text">
          Hellö Admin!!!
          </span>
        <i className="fas fa-sign-out-alt menu-admin-sign-out"></i>
      </div>
    </nav>
  )
}
Navbar.propTypes = {
  title: PropTypes.string.isRequired
}
Navbar.defaultProps = {
  title: "Default App"
}
export default Navbar;
