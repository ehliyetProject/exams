import React, { Component } from 'react';
import './App.css';
import Navbar from "./layout/Navbar";
import AddQuestion from "./forms/AddQuestion";
import UpdateQuestion from "./forms/UpdateQuestion";
import Questions from "./components/Questions";
import ExamTypes from "./components/ExamTypes";
import UpdateExamTypes from "./forms/UpdateExamTypes";
import AddExamType from "./forms/AddExamType";
import NotFound from "./pages/NotFound";
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
import ExamDates from './components/ExamDates';
import AddExamDate from './forms/AddExamDate';
import UpdateExamDate from './forms/UpdateExamDate';
import Login from './components/Login';

class App extends Component {
  render() {
   
    return (
      <Router>
        <div className="container-fluid">
         <Navbar title = "Logo"/>
          <hr/>
          </div>
          <div className="container">
          <Switch>
            <Route exact path = "/questions" component = {Questions} />
            <Route exact path = "/login" component = {Login} />
            <Route exact path = "/addQuestion" component = {AddQuestion} />
            <Route exact path = "/addExamType" component = {AddExamType} />
            <Route exact path = "/AddExamDates" component = {AddExamDate} />
            <Route exact path = "/ExamDates" component = {ExamDates} />
            <Route exact path = "/examTypes" component = {ExamTypes} />
            <Route exact path = "/edit/:id" component = {UpdateQuestion} />
            <Route exact path = "/editExamType/:id" component = {UpdateExamTypes} />
            <Route exact path = "/editExamDate/:id" component = {UpdateExamDate} />
            <Route component = {NotFound} />
          </Switch>
          </div>
      </Router>

      

     
     
      
    );
  }
}

export default App;
