import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ExamDatesConsumer from "../contexts/ExamDateContext";
import axios from "axios";
import {Link} from "react-router-dom";

class ExamDate extends Component {
  static defaultProps = {
    id : "Bilgi Yok",
    examDate : "Bilgi Yok"
  }
  
  onDeleteExamDate = async (dispatch,e) => {
     const {id} = this.props;
     // Delete Request
    await axios.delete(`http://localhost:3004/examDates/${id}`);

    // Consumer Dispatch
    dispatch({type : "DELETE_EXAMDATE",payload:id});
    window.location.reload(); 
  }
  render() {

    // Destructing
    const {id,examDate} = this.props;
    
      return (
            <ExamDatesConsumer>
            {
        value => {
          const {dispatch} = value;
          return (
          <tr>
                <td className="text-center">{id}</td>
                <td className="text-center">{examDate}</td>
                <td>
                <div className="row">
                    <div className="col-lg-6">
                    <button onClick = {this.onDeleteExamDate.bind(this,dispatch)} className = "btn btn-danger btn-block operation-button">
                      Sil
                      </button>
                    </div>
                    <div className="col-lg-6">
                    <Link to = {`editExamDate/${id}`} className = "btn btn-info btn-block operation-button"> Düzenle </Link>
                    </div>
                  </div>
                </td>
              </tr>
          )
        }
      }</ExamDatesConsumer>
   
    )
         }
       }

       ExamDatesConsumer.propTypes = {
  id:PropTypes.string.isRequired,
  examDate : PropTypes.string.isRequired
}
export default ExamDate;
