import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ExamTypesConsumer from "../contexts/ExamTypesContext";
import axios from "axios";
import {Link} from "react-router-dom";

class ExamType extends Component {
  static defaultProps = {
    id : "Bilgi Yok",
    examName : "Bilgi Yok"
  }
  
  onDeleteExamType = async (dispatch,e) => {
     const {id} = this.props;
     // Delete Request
    await axios.delete(`http://localhost:3004/examTypes/${id}`);

    // Consumer Dispatch
    dispatch({type : "DELETE_QUESTİONEXAMTYPES",payload:id});
    window.location.reload(); 
  }
  render() {

    // Destructing
    const {id,examName} = this.props;
    
      return (
            <ExamTypesConsumer>
            {
        value => {
          const {dispatch} = value;
          return (
          <tr>
                <td className="text-center">{id}</td>
                <td className="text-center">{examName}</td>
                <td>
                <div className="row">
                    <div className="col-lg-6">
                    <button onClick = {this.onDeleteExamType.bind(this,dispatch)} className = "btn btn-danger btn-block operation-button">
                      Sil
                      </button>
                    </div>
                    <div className="col-lg-6">
                    <Link to = {`editExamType/${id}`} className = "btn btn-info btn-block operation-button"> Düzenle </Link>
                    </div>
                  </div>
                </td>
              </tr>
          )
        }
      }</ExamTypesConsumer>
   
    )
         }
       }

  ExamTypesConsumer.propTypes = {
  id:PropTypes.string.isRequired,
  examName : PropTypes.string.isRequired
}
export default ExamType;
