import React, { Component } from 'react'
import ExamDate from "./ExamDate";
import ExamDatesConsumer from "../contexts/ExamDateContext";

class ExamDates extends Component {
  render() {
        
    return (
        <div>
            <table className="table table-striped">
                    <thead>
                    <tr>
                        <th className="text-center">ID</th>
                        <th className="text-center">Sınav Tarihi</th>
                        <th className="text-center">Yapılacak İşlem</th>
                    </tr>
                    </thead>
                    
                        <ExamDatesConsumer>
                            {
                            value => {
                                console.log("vfvfvfvf   "+value);
                                const {examDates} = value;
                                    return (
                                        <tbody>
                                        {
                                        examDates.map(examdate => {
                                        return (
                                            <ExamDate
                                                key = {examdate.id}
                                                id = {examdate.id}
                                                examDate = {examdate.examDate}
                                            />
                                                )

                                            })
                                        }
                                        </tbody>
                                                )
                                            }
                            }
                        </ExamDatesConsumer>
                        
            </table>
        </div>
            )
    }
}

export default ExamDates;


