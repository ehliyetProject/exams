import React, { Component } from 'react'
import Question from "./Question";
import QuestionConsumer from "../contexts/QuestionContext";

class Questions extends Component {

    state = {
        filterText:''
    };

    onChangeFilterText = (e) => {
        this.setState({
            filterText: e.target.value
        });
    };

    
  render() {
        
    return (
        <div>
                <input
                value = { this.state.filterText}
                onChange = { this.onChangeFilterText }
                className="form-control form-control-lg" 
                name={"filter"} 
                id={"filter"} 
                placeholder={"Aranacak Soruyu veya ID'sini Giriniz"}/>

            <table className="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col" className="text-center">Soru</th>
                        <th scope="col" className="text-center">Doğru Cevap</th>
                        <th scope="col" className="text-center">Tarih - Sınav Türü</th>
                        <th scope="col" className="text-center">Yapılacak İşlem</th>
                    </tr>
                    </thead>
                    
                        <QuestionConsumer>
                            {
                            value => {
                                const {questions} = value;
                                const questionslist = questions.filter(
                                    question => {
                                        return (question.question.toLowerCase().indexOf(
                                            this.state.filterText.toLowerCase()
                                        )!== -1)
                                        || (question.id.toLowerCase().indexOf(
                                            this.state.filterText.toLowerCase()
                                        ) !== -1)
                                        || (question.examType.toLowerCase().indexOf(
                                            this.state.filterText.toLowerCase()
                                        ) !== -1)
                                        || (question.date.toLowerCase().indexOf(
                                            this.state.filterText.toLowerCase()
                                        ) !== -1)
                                    }
                                );
                                    return (
                                        <tbody> 
                                        {
                                        questionslist.map(questions => {
                                        return (
                                            <Question
                                                key = {questions.id}
                                                id = {questions.id}
                                                question = {questions.question}
                                                optionA = {questions.optionA}
                                                optionB = {questions.optionB}
                                                optionC = {questions.optionC}
                                                optionD = {questions.optionD}
                                                trueOption = {questions.trueOption}
                                                date = {questions.date}
                                                examType = {questions.examType}
                                            />
                                                )

                                            })
                                        }
                                        </tbody>
                                                )
                                            }
                            }
                        </QuestionConsumer>
                        
            </table>
        </div>
            )
    }
}

export default Questions;

