import React, { Component } from 'react'
import PropTypes from 'prop-types'
import QuestionConsumer from "../contexts/QuestionContext";
import axios from "axios";
import {Link} from "react-router-dom";



class Question extends Component {
  static defaultProps = {
    question : "Bilgi Yok",
        optionA : "Bilgi Yok",
        optionB : "Bilgi Yok",
        optionC : "Bilgi Yok",
        optionD : "Bilgi Yok",
        trueOption: "Bilgi Yok",
        date: "Bilgi Yok",
        examType:"Bilgi Yok"
  }
  
  onDeleteQuestion = async (dispatch,e) => {
     const {id} = this.props;
     // Delete Request
    await axios.delete(`http://localhost:3004/Questions/${id}`);

    // Consumer Dispatch
    dispatch({type : "DELETE_QUESTİON",payload:id});
  }
  render() {

    // Destructing
    const {id,question,trueOption,date,examType} = this.props;
    // ,optionA,optionB,optionC,optionD
    
      return (
            <QuestionConsumer>
            {
        value => {
          const {dispatch} = value;
          return (
          <tr>
                <td className="text-center">{question}</td>
                <td className="text-center">{trueOption}</td>
                <td className="text-center">{date} - {examType}</td>
                <td className="text-center">
                  <div className="row">
                    <div className="col-lg-6">
                    <a key={id} onClick = {this.onDeleteQuestion.bind(this,dispatch)} className = "btn btn-danger btn-block operation-button">Sil</a>
                    </div>
                    <div className="col-lg-6">
                    <Link to = {`edit/${id}`} className = "btn btn-info btn-block operation-button"> Düzenle </Link>
                    </div>
                  </div>
                </td>
              </tr>
          )
        }
      }</QuestionConsumer>
   
    )
         }
       }

QuestionConsumer.propTypes = {
  id:PropTypes.string.isRequired,
  question : PropTypes.string.isRequired,
  optionA : PropTypes.string.isRequired,
  optionB : PropTypes.string.isRequired,
  optionC : PropTypes.string.isRequired,
  optionD : PropTypes.string.isRequired,
  trueOption : PropTypes.string.isRequired,
  date : PropTypes.string.isRequired,
  examType : PropTypes.string.isRequired
}
export default Question;
