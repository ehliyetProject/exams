import React, { Component } from 'react'
import ExamType from "./ExamType";
import ExamTypesConsumer from "../contexts/ExamTypesContext";

class ExamTypes extends Component {
  render() {
        
    return (
        <div>
            <table className="table table-striped">
                    <thead>
                    <tr>
                        <th className="text-center">ID</th>
                        <th className="text-center">Sınav Türü Adı</th>
                        <th className="text-center">Yapılacak İşlem</th>
                    </tr>
                    </thead>
                    
                        <ExamTypesConsumer>
                            {
                            value => {
                                console.log(value);
                                const {examTypes} = value;
                                    return (
                                        <tbody>
                                        {
                                        examTypes.map(examType => {
                                        return (
                                            <ExamType
                                                key = {examType.id}
                                                id = {examType.id}
                                                examName = {examType.examName}
                                            />
                                                )

                                            })
                                        }
                                        </tbody>
                                                )
                                            }
                            }
                        </ExamTypesConsumer>
                        
            </table>
        </div>
            )
    }
}

export default ExamTypes;


