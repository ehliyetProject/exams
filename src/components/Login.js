import "../css/Login.css";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import React, { Component } from "react";
import axios from "axios";
// import Questions from "./components/Questions";

export default class Login extends Component {
    constructor(props) {
      super(props);
      this.state = {
        email: "",
        password: ""
      };
    }
    async  loginService (userBody) {
        let axiosConfig = {
            headers:{
                "Content-Type": "application/json;charset=UTF-8",
                "Access-Control-Allow-Origin": "*"
            }
        };
        await axios.post("http://localhost:5000/login/",userBody,axiosConfig).then(res => {
                console.log("res: " +res)
                console.log(res.status)
                if(res.status ===201 || res.status === 200)
                {
                    alert("giriş başarılı");
                    window.open('http://localhost:3000/questions', '_blank');
                    // this.props.history.push("/questions");
                    // window.location.reload("/questions");
                }else{
                    alert("hatalı girişa");
                    window.location.reload();
                }
            })
        //     .catch(err => {
        //         alert("Hatalı Girişb"+ err);
        //         console.log("err:" +err)
        //         window.location.reload()
        // })
    }

    validateForm() {
      return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
      this.setState({
        [event.target.id]: event.target.value
      });
    }

    handleSubmit = event => {
      event.preventDefault();
      this.loginService(JSON.stringify(this.state))
    }

    render() {
      return (
        <div className="Login">
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="email">
              <Form.Control
                autoFocus
                type="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Control
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
              />
            </Form.Group>
            <Button
              block
              disabled={!this.validateForm()}
              type="submit"
            >
              Login
            </Button>
          </Form>
        </div>
      );
    }
  }