import React, { Component } from 'react'
import axios from "axios";

const ExamTypesContext = React.createContext();

// Provider , Consumer
const reducer = (state,action) => {
  switch(action.type) {
    case "DELETE_EXAMTYPES":
       return {
        ...state,
        examTypes: state.examTypes.filter(examType => action.payload !== examType.id)
       }
    case "ADD_EXAMTYPES":
       return {
         ...state,
         examTypes : [...state.examTypes,action.payload]
       }
    case "UPDATE_EXAMTYPES":
       return {
         ...state,
         examTypes: state.examTypes.map(examType => examType.id === action.payload.id ? action.payload : examType)
       }
    default:
      return state
  }
}

export class ExamTypesProvider extends Component {
    state = {
        examTypes: [],
        dispatch : action => {
          this.setState(state => reducer(state,action))
        }
      }
      
  componentDidMount = async () => {
    const Response = await axios.get("http://localhost:3004/examTypes");
    this.setState({
        examTypes: Response.data
    })
  }
      
  render() {
    return (
      <ExamTypesContext.Provider value = {this.state}>
        {this.props.children}
      </ExamTypesContext.Provider>
    )
  }
}
const ExamTypesConsumer = ExamTypesContext.Consumer;

export default ExamTypesConsumer;