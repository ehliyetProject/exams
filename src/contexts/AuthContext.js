import React, { Component } from "react";
const AuthContext = React.createContext();

switch (action.type) {
        case 'LOGIN':
            return {
                isLoggedIn: true,
                username: action.payload.username,
                error: ''
            };
        case 'LOGIN_ERROR':
            return {
                isLoggedIn: false,
                username: '',
                error: action.payload.error
            };
        case 'LOGOUT':
            return {
                isLoggedIn: false,
                username: '',
                error: ''
            };
        default:
            return state;
    }

const reducer = (state,action) => {
    switch(action.type) {
      case "LOG_IN":
         return {
          ...state,
          isLoggedIn: true
         }
      case "LOG_OUT":
         return {
           ...state,
           examDates : [...state.examDates,action.payload]
         }
      case "UPDATE_EXAMDATE":
         return {
           ...state,
           isLoggedIn: false
         }
      default:
        return state
    }
  }

class AuthProvider extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    };
  }

  render() {
    return (
      <AuthContext.Provider
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}
const AuthConsumer = AuthContext.Consumer;

export default AuthConsumer;