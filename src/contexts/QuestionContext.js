import React, { Component } from 'react'
import axios from "axios";

const QuestionContext = React.createContext();

// Provider , Consumer
const reducer = (state,action) => {
  switch(action.type) {
    case "DELETE_QUESTİON":
       return {
        ...state,
        questions: state.questions.filter(question => action.payload !== question.id)
       }
    case "ADD_QUESTİON":
       return {
         ...state,
         questions : [...state.questions,action.payload]
       }
    case "UPDATE_QUESTİON":
       return {
         ...state,
         questions: state.questions.map(question => question.id === action.payload.id ? action.payload : question)
       }
    default:
      return state
  }
}

export class QuestionProvider extends Component {
    state = {
      questions: [],
      examTypes: [],
      examDates: [],
        dispatch : action => {
          this.setState(state => reducer(state,action))
        }
      }
      
  componentDidMount = async () => {
    const Response = await axios.get("http://localhost:3004/Questions");
    const ResponseExamTypes = await axios.get("http://localhost:3004/examTypes");
    const ResponseExamDates = await axios.get("http://localhost:3004/examDates");
    // console.log("Response "+Response)
    // console.log("ResponseExamTypes "+ResponseExamTypes)
    // console.log("ResponseExamDates "+ResponseExamDates)
    this.setState({
      questions: Response.data,
      examTypes: ResponseExamTypes.data,
      examDates: ResponseExamDates.data
    })
  }
      
  render() {
    return (
      <QuestionContext.Provider value = {this.state}>
        {this.props.children}
      </QuestionContext.Provider>
    )
  }
}
const QuestionConsumer = QuestionContext.Consumer;

export default QuestionConsumer;