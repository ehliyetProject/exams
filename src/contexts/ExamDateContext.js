import React, { Component } from 'react'
import axios from "axios";

const ExamDatesContext = React.createContext();

// Provider , Consumer 
const reducer = (state,action) => {
  switch(action.type) {
    case "DELETE_EXAMDATE":
       return {
        ...state,
        examDates: state.examDates.filter(examDate => action.payload !== examDate.id)
       }
    case "ADD_EXAMDATE":
       return {
         ...state,
         examDates : [...state.examDates,action.payload]
       }
    case "UPDATE_EXAMDATE":
       return {
         ...state,
         examDates: state.examDates.map(examDate => examDate.id === action.payload.id ? action.payload : examDate)
       }
    default:
      return state
  }
}

export class ExamDatesProvider extends Component {
    state = {
        examDates: [],
        dispatch : action => {
          this.setState(state => reducer(state,action))
        }
      }
      
  componentDidMount = async () => {
    const Response = await axios.get("http://localhost:3004/examDates");
    this.setState({
        examDates: Response.data
    })
  }
      
  render() {
    return (
      <ExamDatesContext.Provider value = {this.state}>
        {this.props.children}
      </ExamDatesContext.Provider>
    )
  }
}
const ExamDatesConsumer = ExamDatesContext.Consumer;

export default ExamDatesConsumer;